from bs4 import BeautifulSoup

with open("testreport.html", "r", encoding="utf-8") as file:
  content = file.read()

soup = BeautifulSoup(content, "html.parser")


def checking(risk_class):
  return str(soup.find('td', class_=risk_class).find_parent('tr').find_all('td')[1].find('div').text)


# print(checking('risk-3')) # pas de vuln
print(checking('risk-2')) # vuln
