# DockerAndGitlabCICD

## PARTIE 1 : Docker 🐋
### Exercice 1 : Une application statique dockerisée

Dans l'exercice 1 on a la volontée de créer une application web statique simple, qui nécéssite une phase de build en production, j'ai pour cela réutiliser mon portfolio que j'avais codé il y a plus d'un an (PS : il n'est pas à jour ➡️ [clique ici](https://my-portfolio-nabilainas.vercel.app/))

> Voici où ce trouve mon application web statique dans le repertoire ➡️ [clique ici](my-portfolio/)

Maintenant que tu sais qu'elle est l'application que j'ai codé et où elle se trouve dans le répertoire voyons voir comment récupérer tout ça en local et comment l'éxécuter


#### Exécution en local (Optionnel)

> Afin de tester l'application en local sans passer par docker, on peut suivre ces étapes, si tu ne veux pas suivre cette étape facultative tu peux passer à la suivante

|Prérequis     |Version |
|--------------|--------|
|Docker Desktop|v4.26.1 |
|Node JS       |v20.10.0|

Dans un premier temps on va cloner le répertoire en local

```bash
git clone https://gitlab.com/nabilainas/project.git && cd project/my-portfolio
```

Boom ! on est directement à la racine de notre application web, plus qu'une seule étape avant de l'éxécuter l'application en local sans passer par docker

```bash
npm install && npm run dev -- --host
```

#### Exécution via un conteneur avec un volume pour changer les données

> Ici nous allons éxécuter notre application transformée en image via un conteneur et lié un volume pour pouvoir modifier les données à l'interieur, nous n'avons pas besoin de donner les prérequis pour ton pc ici.

```bash
git clone https://gitlab.com/nabilainas/project.git && cd project/my-portfolio
```

Boom ! on est directement à la racine de notre projet, maintenant nous allons créer notre image via le [Dockerfile.dev](my-portfolio/Dockerfile.dev)
On entre ensuite cette commande pour créer l'image

```bash
docker build -t nabilainas/my-dev-portfolio:1.0 -f Dockerfile.dev .
```
On entre ensuite cette commande pour lancer le conteneur et lier le notre volume pour pouvoir modifier des informations.

```bash
docker run -d --name mydevcontainer -p 5173:5173 -v CHEMIN_ABSOLU_DU_PROJET/my-portfolio/src/components:/my-portfolio/src/components nabilainas/my-dev-portfolio:1.0
```
⚠️ *CHEMIN_ABSOLU_DU_PROJET* doit être remplacé par le chemin absolu vers votre projet sur votre système local. ⚠️

> Par exemple modifie les informations perso dans le fichier **Presentation.jsx** en changeant mon prénom par le tiens puis **redémarre le conteneur** (docker ne prend pas en compte le rafraîchissement automatique de la page)

##### Explication du fichier Dockerfile.dev en détail

j'ai hierarchiser les commande en fonction de celle qui prennent le plus de temps jusque celles qui en demande moins afin d'optimiser le Build

```Dockerfile
FROM node:alpine
```
l'image de base utilisée pour construire cette image Docker est node:alpine, qui est une version allégée de l'image Node.js basée sur Alpine Linux. Cela permet de réduire considérablement la taille de l'image Docker finale par rapport à d'autres images de Node.js

```Dockerfile
WORKDIR /my-portfolio
```
Cette instruction définit le répertoire de travail à l'intérieur du conteneur Docker (/my-portfolio).

```Dockerfile
COPY . .
```
Cette instruction copie tous les fichiers du répertoire de construction actuel dans le répertoire de travail du conteneur (/my-portfolio). Je me suis déjà occupé des fichiers indésirables dans le fichier [.dockerignore](my-portfolio/.dockerignore)

```Dockerfile
RUN npm ci
```
Cette instruction utilise npm ci au lieu de npm install pour installer les dépendances de manière plus efficace en utilisant le fichier package-lock.json.

```Dockerfile
EXPOSE 5173
```
Cette instruction expose le port 5173 du conteneur Docker. Cela permet à des applications externes d'accéder à des services exécutés sur ce port, bien que cela ne publie pas effectivement le port sur l'hôte lui-même. Pour accéder à ce port depuis l'extérieur du conteneur, il doit être mappé lors de l'exécution du conteneur avec l'option -p ou --publish.

```Dockerfile
CMD [ "npm", "run","dev", "--", "--host" ]
```
Cette instruction spécifie la commande par défaut à exécuter lorsque le conteneur démarre. Dans ce cas, il exécute la commande npm run dev -- --host. qui lance la version de developpement de l'application et qui expose sur le réseau local l'application.

```Dockerfile
LABEL maintainer="@nabilainas"
LABEL date="28/12/2023"
```
Ces instructions sont des métadonnées à une image Docker. Elles sont principalement utilisées pour fournir des informations supplémentaires sur l'image.
ici c'est le créateur et la date de création de l'image.

##### Explication des commandes de build d'image et de run du conteneur en détail 

```bash
docker build -t nabilainas/my-dev-portfolio:1.0 -f Dockerfile.dev .
```

docker build est la commande principale pour construire des images Docker à partir d'un Dockerfile et d'un contexte de construction. 

- L'option *-t* est utilisée pour spécifier un tag ou un nom pour ton image Docker. Dans ce cas, l'image sera taguée avec le nom *nabilainas/my-dev-portfolio* et la version *1.0*. 

- L'option *-f* permet de spécifier le nom du fichier Dockerfile à utiliser pour la construction de l'image. Ici, le fichier Dockerfile utilisé pour la construction est nommé *Dockerfile.dev*. 

- Le *.* à la fin de la commande spécifie le contexte de construction. Cela indique à Docker où chercher les fichiers nécessaires pour la construction de l'image. Docker prendra tous les fichiers du répertoire actuel comme contexte de construction, sauf les fichiers spécifiés dans le [.dockerignore](my-portfolio/.dockerignore).

```bash
docker run -d --name mydevcontainer -p 5173:5173 -v CHEMIN_ABSOLU_DU_PROJET/my-portfolio/src/components:/my-portfolio/src/components nabilainas/my-dev-portfolio:1.0
```

docker run est la commande pour exécuter un conteneur Docker à partir d'une image. 

- L'option *-d* Indique à Docker de démarrer le conteneur en mode détaché, ce qui signifie qu'il s'exécute en arrière-plan. 

- L'option *--name* donne un nom spécifique au conteneur, dans ce cas, *mydevcontainer*. *-p 5173:5173:* Mappe le port 5173 du conteneur au port 5173 de l'hôte (on aurait pu mapper un autre port pour l'hôte, j'aime juste bien l'uniforimté.). 

- L'option *-v* permet le montage d'un volume en liant le répertoire local au répertoire /my-portfolio/src/components dans le conteneur. Cela permet d'échanger des fichiers entre le système hôte et le conteneur, **CHEMIN_ABSOLU_DU_PROJET** doit être remplacé par le chemin absolu vers votre projet sur votre système local. Le nom *nabilainas/my-dev-portfolio:1.0* Spécifie l'image Docker à partir de laquelle le conteneur sera démarré.

### Exercice 2 : Docker Multistage Build
#### quel est l'intérêt d'un build ?

Le concept général d'un "build" dans le développement concerne la transformation des fichiers sources en fichiers optimisés et prêts pour la production.
Le processus de build compile les langages en versions compatibles avec les environnements de déploiement, le build inclus l'inclusion des dépendances, afin de garantir que toutes les dépendances nécessaires sont correctement intégrées dans l'application finale. Mais le plus important, Le processus de build permet de regrouper, minifier et compresser les fichiers sources. Cela réduit leur taille, ce qui peut accélérer le temps de chargement de l'application dans le navigateur et améliorer les performances globales, cela améliore aussi la portabilité de l'application.

#### Exécution du conteneur

> Ici nous allons éxécuter notre application "buildé" transformée en image via un conteneur, nous n'avons pas besoin de donner les prérequis pour ton pc ici.

Nous allons créer notre image via le [Dockerfile.prod](my-portfolio/Dockerfile.prod), on entre ensuite cette commande pour créer l'image
```bash
docker build -t nabilainas/my-prod-portfolio:1.0 -f Dockerfile.prod .
```

On entre ensuite cette commande pour lancer le conteneur et lier le notre volume pour pouvoir modifier des informations.
```bash
docker run -d --name myprodcontainer -p 80:80 nabilainas/my-prod-portfolio:1.0
```

#### Explication du fichier Dockerfile.prod en détail
J'ai fais les mêmes optimisations que sur l'exercice 1, afin de ne pas surcharger le fichier README.md je ne vais pas (re)développer les points vus précedemment.

```Dockerfile
FROM node:alpine AS builder
WORKDIR /my-portfolio
COPY . .
RUN npm ci && npm run build
```

dans cette partie le plus important est l'ajout de **AS builder** et **npm run build**:
- premièrement l'utilisation de *AS*  est utile pour diviser le processus de construction en étapes logiques, isolant la construction des dépendances, la compilation, ou la génération de fichiers spécifiques pour pouvoir les réutiliser dans l'étape suivante, contribuant à une meilleure organisation et à des images Docker plus efficaces. ici on aura le dossier **my-portfolio** dans **builder**.

- On a ensuite npm run build qui va créer un dossier **dist** avec l'application compilée avec des fichiers optimisés prêts à être déployés en production, comme expliqué précedemment.


```Dockerfile
FROM nginx:alpine
WORKDIR /usr/share/nginx/html
COPY --from=builder /my-portfolio/dist .
EXPOSE 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]
```

dans cette partie on utilise comme base l'image **nginx:alpine** qui est une version allégée de l'image Nginx basée sur Alpine Linux, nginx est un serveur web open source 
- *WORKDIR* En spécifiant /usr/share/nginx/html, cela configure le répertoire où Nginx attend les fichiers statiques à servir.
- *COPY* Cette instruction copie des fichiers depuis l'étape précédente du Dockerfile, qui à été tag **builder**, dans le répertoire actif de cette étape de construction. Plus précisément, elle copie les fichiers générés par la commande npm run build dans /my-portfolio/dist de l'étape de construction, vers le répertoire /usr/share/nginx/html de cette étape.
- *ENTRYPOINT* définit la commande exécutée à chaque fois que le conteneur démarre, et cette commande ne peut pas être substituée de la même manière que CMD qui définit une commande par défaut qui peut être remplacée lors du démarrage du conteneur. cela permet de se prévenir des mauvaise manipulation ou l'utilisation d'autres commandes via la ligne de commande. Cette instruction spécifie la commande à exécuter lorsque le conteneur démarre. Ici, elle démarre le serveur Nginx en mode daemon (daemon off; indique de ne pas fonctionner en arrière-plan).

#### Explication des commandes de build d'image et de run du conteneur en détail

Les commandes sont les mêmes utilisés dans l'Exercice 1. 

### Exercice 3 : Traefik & Docker Compose

> On utilise dans le fichier compose la version 3.8, tu dois avoir la version de Docker Engine compatible, soit la version 19.03.0 minimum

|Prérequis     |Version       |
|--------------|--------------|
|Docker Engine |19.03.0 (ou +)|
|Compose       |3.8 (latest)  |

#### Exécution du conteneur

On lance la commande dans le repertoire **my-portfolio/** pour éxecuter cette stack de services afin de créer une application, 

```bash
docker-compose up -d
```

On peut maintenant se connecter sur le site web via cette adresse depuis votre réseau local à l'adresse web.docker.localhost ➡️ [clique ici](my-portfolio.localhost)

#### Explication du fichier docker-compose.yml en détail

> Dans cette partie je vais expliquer mon fichier [docker-compose.yml](./my-portfolio/docker-compose.yml)

Le fichier [docker-compose.yml](./my-portfolio/docker-compose.yml) définit les services qui compose une application et chaque "service" contient les instructions pour construire et gérer un conteneur.

Le premier service est traefik, c'est un reverse proxy qui va nous permettre dans ce cas d'usage de gérer le routage du trafic entrant vers le service web pour le portfolio.

il va aussi servire de load balancer afin de répartir les charges, si par exemple je veux déployer plusieurs application web sous le même nom de domaine j'entre la commande:

```bash
docker-compose up -d --scale web=2
```

et qu'ensuite je supprime un conteneur, le site sera toujours accessible à cette adresse.

Pour rentrer + dans le détail, dans l'énoncé on à dans les critères d'évaluation, La pertinance des choix fait pour traefik, que je vais détailler ci dessous

```yaml
services:
  reverse-proxy:
    image: traefik:v2.10
    command: --providers.docker
    ports:
      - 80:80
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
```

Dans ce use case la configuration de Traefik n'a pas besoin d'être très élaborée car cela serait inutile, mes choix se basent sur la simplicité des options pour répondre à l'énoncé.

- **reverse-proxy** est le nom du service (assez explicite)
- **--providers.docker** est la commande indispensable qui indique à Traefik d'utiliser Docker comme fournisseur de configuration pour router le trafic vers les services.
- **--api.insecure=true** pouvait être utilisée pour activer l'interface web de Traefik mais cela n'a pas d'importance dans ce use case, j'ai mapper les ports 80 et 80.
- On créer ensuite un lien avec le démon docker en créant un volume rattaché à **/var/run/docker.sock** ce qui permet à Traefik de recevoir des notifications en temps réel sur les changements dans l'environnement Docker. Ainsi, Traefik peut automatiquement mettre à jour sa configuration de routage en fonction des événements, comme le déploiement de nouveaux conteneurs, la modification des étiquettes Docker (labels), etc. Cela permet à Traefik de s'adapter dynamiquement aux changements dans les services Docker sans nécessiter de redémarrage ou de reconfiguration manuelle. Voici les choix que j'ai fait pour la création du reverse proxy Traefik

Maintenant pour l'application web on a:

```yaml
 web:
    build: 
      context: .
      dockerfile: Dockerfile.prod
    labels:
      - "traefik.http.routers.web.rule=Host(`my-portfolio.localhost`)"
```

- **web** est le nom du service, **build** défini dans ce contexte le chemin du  pour créer une image à partir d'un Dockerfile.
- **labels** permet de rajouter des métadonnées pour ajouter des informations supplémentaires à notre conteneur, ici il permet d'indiquer à Traefik comment router le trafic HTTP entrant pour le service web en fonction de l'hôte de l'URL. ici Traefik route le trafic entrant lorsqu'il reçoit une demande ayant pour hôte my-portfolio.localhost vers le service nommé "web" dans la configuration de Traefik.

## Partie 2: GitLab CI/CD 🦊
### Exercice 1 : Intégration et Déploiement Continu avec Auto DevOps
#### Configurer le repository GitLab avec Auto DevOps / Choix de configuration

En suivant la [documentation](https://docs.gitlab.com/ee/topics/autodevops/), on a tout d'abord, les différentes étapes pour activer autodevops, pour cela on doit aller dans *Settings > CI/CD > ☑️ Default to Auto DevOps pipeline > Save changes*

![enable-autodevops](./assets/enable-autodevops.png)

Autodevops permet de pouvoir utiliser une pipeline CI/CD préconfigurée sans avoir et configurer un fichier **.gitlab-ci.yml**, en utilisant des templates prédéfinies.

> Après avoir activé Auto DevOps on a un déclanchement de la Template Auto DevOps qui ressemble à ça :

![ad-pipe](./assets/ad-pipe.png)

La pipeline échoue car nous n'avons pas les configuration requise pour l'instant ➡️[voir pipeline](https://gitlab.com/nabilainas/project/-/pipelines/1136299008).


Nous allons voir de plus près ce qui se cache derrière ces templates pour savoir ce que l'on peut utilisé dans notre use case. Voici la template utilisé lors de la pipeline précédente ➡️ [voir template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml)

On a tous ces stages qui correspondent à la pipeline précédente :

![ad-stages](./assets/ad-stages.png)

Dans ce use case on a seulement besoin du stage de Build, Test et de Deploy, on va donc voir les diférrérentes templates correspondante à celles-ci pour pouvoir les inclure dans notre fichier **.gitlab-ci.yml** et ainsi utiliser seulement les fonctions nécéssaires (si possible), voici les différentes templates :

![ad-templates](./assets/ad-templates.png)

Nous allons donc seulement nous intéresser à Jobs/Build.gitlab-ci.yml, Jobs/Deploy.gitlab-ci.yml et Jobs/Code-Quality.gitlab-ci.yml.

Nous allons premièrement rencontrer 1 erreure avec le build que je n'ai pas réussi à résoudre et qui fait que l'on ne pourra pas utiliser ce template dans la limite de notre infrastructure:

![build-not-found](./assets/build-not-found.png)

l'erreure */build/build.sh not found*. On peut voir dans le code que la template est pas très compliquée à comprendre : ➡️ [clique ici](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Build.gitlab-ci.yml), on ne se préoccupe pas du job *build_artifact* car il s'éxécute que si l'on veut déployer sur une instance AWS EC2. On a dans job build la vérification si un tag est présent ou non et ensuite on a l'éxécution du script /build/build.sh, je tiens à preciser que la documentation AutoDevops est très subjective, on rentre pas assez dans le détail, j'ai pas pu trouver des information donc j'ai fontionné par tatonnement, j'ai d'abord créer un dossier build avec le fichier build.sh a la racine de mon projet sans succès, je l'ai ensuite mis dans mon runner sans succès (c'est un chemin absolu), j'ai abandonné en voyant que sur le web je n'ai pas trouvé + d'infos ➡️ [voir infos](https://superuser.com/questions/1417287/gitlab-runner-bash-line-526-build-build-sh-no-such-file-or-director)

Ensuite pour le deploy, ➡️ [clique ici](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml), ce template est inutile car on peut seulement déployer sur une instance kubernetes, donc je ne vais pas l'utiliser. 

j'ai ensuite utiliser le test on a la template ➡️ [clique ici](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml), ce template permet de verifier la qualité du code, je n'ai pas rencontré d'erreur, donc il est assez facile à mettre en place.


J'ai donc pratiquement tout fais à la main, je trouve qu'Auto DevOps est plutôt inutile dans ce cas d'usage, en effet AutoDevops serait utile dans un cas de déploiement sur des conteneur orchestrer par Kubernetes ou bien des plateforme Cloud.

#### Architecture de l'infrastructure

Je vais expliquer l'architecture de mon infra pour l'éxécution des pipelines

> voici l'infrastructure :

![archi](./assets/archi.png)

J'ai opter pour un executeur **shell** afin d'avoir un contrôle complet sur les commandes qui vont être éxécutées (bash), cette exécuteur est une machine virtuelle ubuntu 22.04 WSL2 avec docker, python et node installé (voir dans la prochaine partie), j'ai utiliser aussi utiliser mon registry docker personnel afin de stocker les images.

Lors d'un push sur le repository distant, une pipeline va s'éxécuter

#### Création de son propre runner et l'éxécuter

Je vais créer mon propre runner sur un ubuntu sur WSL 2 afin d'exécuter mes pipelines CI/CD, comme vu précedemment dans l'architecture de mon infra


Je commence par récupérer les mises à jour puis j'installe docker dessus
```bash
apt-get install docker.io
```

cette commande permet de récupérer et installer les packages gitlab-runner
```bash
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
```

ensuite j'installe le runner gitlab
```bash
apt-get install gitlab-runner
```

Ensuite j'ajoute mon runner en tant qu'utilisateur du groupe docker pour ne pas avoir de problèmes de droits
```bash
usermod -aG docker gitlab-runner
```

je lance ensuite cette commande qui est tout simplement un copier-coller de la commande à utiliser quand un créer le runner sur gitlab
```bash
gitlab-runner register \
  --url https://gitlab.com \
  --token glrt-mpP2jp4oYdudMyH5j9Pr 
```

Le runner est bien configuré comme on peut le voir ici :

![config-proof](./assets/config-proof.png)

> PS : on ne doit pas oublier de desactivé l'utilisation des shared runners

#### Fichier .gitlab-ci.yml

Voici à quoi ressemble mon fichier [.gitlab-ci.yml](./.gitlab-ci.yml) pour cet exercice:

```yml
stages:
  - build
  - test
  - staging

before_script:
  - docker login registry.gitlab.com -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD

include:
  - template: Jobs/Code-Quality.gitlab-ci.yml

build:
  stage: build
  script:
    - cd my-portfolio/
    - docker build -t nabilainas/my-prod-portfolio:1.0 -f Dockerfile.prod .
    - echo "build successful"

staging:
  stage: staging
  script:
    - cd my-portfolio/
    - docker-compose up -d
    - echo "Container deployed staging for tests go to --> http://my-portfolio.localhost"
  environment: staging
  when: on_success
```

##### Explication des différents jobs:

Tout d'abord on défini les différents jobs qui vont être exécutés : build - test - deploy

Avant chaque script on se connecte au registry local pour pouvoir pull les images (utile seulement pour le job de test *Jobs/Code-Quality.gitlab-ci.yml* )

Ensuite on inclus le template *Jobs/Code-Quality.gitlab-ci.yml* d'AutoDevops pour pouvoir l'éxécuter dans notre pipeline

Le job build va permettre de créer une image à partir de notre [Dockerfile.prod](./my-portfolio/Dockerfile.prod) et la stocker dans notre registry privé, mon docker compose fait déjà le build dans le job *staging* mais comme vous avez demander une phase de build je le met, ce job ne sera pas dans le prochain exo.

Le job staging va déployer la version de production via le fichier [docker-compose.yml](./my-portfolio/docker-compose.yml) en local sur l'adresse *http://my-portfolio.localhost*,
dans son environnement de *staging*.

##### Preuve
Voici la pipeline , ➡️ [clique ici](https://gitlab.com/nabilainas/project/-/pipelines/1145885638)

![pipe-proof-exo1](./assets/pipe-proof-exo1.png)
![pipe-proof-exo1-2](./assets/pipe-proof-exo1-2.png)
![pipe-proof-exo1-4](./assets/pipe-proof-exo1-4.png)
![pipe-proof-exo1-3](./assets/pipe-proof-exo1-3.png)

### Exercice 2 : Déploiement en Staging et Production
#### Fichier .gitlab-ci.yml

Voici à quoi ressemble mon fichier [.gitlab-ci.yml](./.gitlab-ci.yml) pour cet exercice:

```yml
stages:
  - test
  - staging
  - production

before_script:
  - docker login registry.gitlab.com -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD

include:
  - template: Jobs/Code-Quality.gitlab-ci.yml

staging:
  stage: staging
  script:
    - docker-compose up -d
    - echo "Container deployed staging for tests go to --> http://my-portfolio.localhost"
  environment: staging
  when: on_success

pages:
  stage: production
  script:
    - cd my-portfolio/
    - npm ci && npm run build
    - mv dist ../public
  artifacts:
    paths:
      - public
  environment: production
  when: manual
```

##### Explication des différents jobs:

On a un nouveau job *production* et on a supprimé le job build comme éxpliqué précedemment,

On a le job pages qui permet de déployer des applications sur **gitlab pages**, pour cela on fait le build en local puis on met en output dans les artifact le contenu du build dans le dossier public, comme vu dans la [documentation](https://docs.gitlab.com/ee/user/project/pages/).

##### Preuve:
Voici la pipeline , ➡️ [clique ici](https://gitlab.com/nabilainas/project/-/pipelines/1145888423)

![pipe-proof-exo2](./assets/pipe-proof-exo2.png)
![pipe-proof-exo2-1](./assets/pipe-proof-exo2-1.png)
![prod-link](./assets/prod-link.png)

### Exercice 3 : Vulnérabilités ?
#### Fichier .gitlab-ci.yml

Voici à quoi ressemble le fichier final [.gitlab-ci.yml](./.gitlab-ci.yml):

```yml
stages:
  - test
  - scan
  - staging
  - production

before_script:
  - docker login registry.gitlab.com -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD

include:
  - template: Jobs/Code-Quality.gitlab-ci.yml

dev-app-for-scan:
  stage: scan
  script:
    - cd my-portfolio/
    - docker build -t nabilainas/my-dev-portfolio:1.0 -f Dockerfile.dev .
    - docker run -d --name mydevcontainer -p 5173:5173 nabilainas/my-dev-portfolio:1.0

scan:
  stage: scan
  script:
    - docker run -v $(pwd):/zap/wrk/:rw -t ghcr.io/zaproxy/zaproxy:stable zap-baseline.py -t http://host.docker.internal:5173 -g gen.conf -r testreport.html || true
  artifacts:
    paths:
      - testreport.html
  needs:
    - job: dev-app-for-scan
  allow_failure: true

check_vulnerabilities:
  stage: scan
  script:
    - res=$(python3 scan.py)
    - |
      if [ "$res" != "0" ]; then
        echo "Vulnerabilities found, check artifact testreport.html"
        exit 1
      else
        echo "No vulnerability found"
      fi
  needs:
    - job: scan


staging:
  stage: staging
  script:
    - cd my-portfolio/
    - docker-compose up -d
    - echo "Container deployed staging for tests go to --> http://my-portfolio.localhost"
  environment: staging
  when: on_success

pages:
  stage: production
  script:
    - cd my-portfolio/
    - npm ci && npm run build
    - mv dist ../public
  artifacts:
    paths:
      - public
  environment: production
  when: manual
```

##### Explication des différents jobs:

On a mis en place un nouveau stage **scan** découpé en 3 différents jobs **dev-app-for-scan** --> **scan** --> **check_vulnerabilities**

le stage scan va permettre comme demandé dans l'enoncé de scanner le code puis de bloquer la pipeline si il trouve des vulnérabilités dans le code.

Le job **dev-app-for-scan** va créer une image de la derniere version du code puis utiliser cette image pour déployer un conteur accessible en local à l'adresse *http://host.docker.internal:5173* pour les runners depuis la machine virtuelle ubuntu, cette adresse va permettre au scan d'avoir une adresse sur laquelle faire le scan.

Le job **scan** va lancer un conteneur avec l'image zaproxy:stable puis va exécuter le fichier zap-baseline.py qui va faire le scan sur l'adresse *http://host.docker.internal:5173*, puis va donner en sortie le rapport des vulnérabilités dans le fichier testreport.html

voici à quoi il ressemble :
![scan-report](./assets/scan-report.png)

Pour vérifier si l'application contient des vulnérabilités je vais utiliser le script python [scan.py](./scan.py), je vais scraper le site avec *BeautifulSoup*, et récupérer le nombre de vulnérabilités, je vais expliquer + en détail juste après, voici à quoi ressemble mon code :

```python
from bs4 import BeautifulSoup

with open("testreport.html", "r", encoding="utf-8") as file:
  content = file.read()

soup = BeautifulSoup(content, "html.parser")
def checking(risk_class):
  return str(soup.find('td', class_=risk_class).find_parent('tr').find_all('td')[1].find('div').text)

# print(checking('risk-3')) # pas de vuln
print(checking('risk-2')) # vuln
```

Mon script python va premierement lire le fichier **testreport.html** récupéré grâce au job **scan**, ensuite je vais récupérer le nombre de vulnérabilités selon une class via la fonction *checking* sur ce tableau:

```html
<tbody>
  <tr>
    <th width="45%" height="24">Risk Level</th>
    <th width="55%" align="center">Number of Alerts</th>
  </tr>
  <tr>
    <td class="risk-3">
      <div>High</div>
    </td>
    <td align="center">
      <div>0</div>
    </td>
  </tr>
  <tr>
    <td class="risk-2">
      <div>Medium</div>
    </td>
    <td align="center">
      <div>3</div>
    </td>
  </tr>
  <tr>
    <td class="risk-1">
      <div>Low</div>
    </td>
    <td align="center">
      <div>2</div>
    </td>
  </tr>
  <tr>
    <td class="risk-0">
      <div>Informational</div>
    </td>
    <td align="center">
      <div>4</div>
    </td>
  </tr>
  <tr>
    <td class="risk--1">
      <div>False Positives:</div>
    </td>
    <td align="center">
      <div>0</div>
    </td>
  </tr>
</tbody>
```

Au lieu d'essayer de trouver des vulnérabilités et de les corriger, j'ai dans la premiere pipeline exécuter la fonction **print(checking('risk-3'))** qui correspond aux vulnérabilités HIGH qui sont égale à 0 et dans la seconde pipeline **print(checking('risk-2'))** qui correspond aux vulnérabilités MEDIUM qui sont égale à 3, le script renvoie le nombre de vulnérabilités (ici dans un environnement de test cela renvoie 0 ou 3)

Le job **check_vulnerabilities** va stocker dans une variable le résultat de l'éxécution du script [scan.py](./scan.py), puis va comparer le résultat, si il est différent de 0 cela renvoie une erreure ce qui fait crash la pipeline, sinon la pipeline réussi.


##### Preuve

Voici la pipeline quand il n'y a pas de vulnérabilités , ➡️ [clique ici](https://gitlab.com/nabilainas/project/-/pipelines/1145899522)

![pipe-proof-exo3-novuln](./assets/pipe-proof-exo3-novuln.png)
![pipe-proof-exo3-novuln-1](./assets/pipe-proof-exo3-novuln-1.png)


Voici la pipeline quand il y a des vulnérabilités , ➡️ [clique ici](https://gitlab.com/nabilainas/project/-/pipelines/1145900936)

![pipe-proof-exo3-vuln](./assets/pipe-proof-exo3-vuln.png)
![pipe-proof-exo3-vuln-1](./assets/pipe-proof-exo3-vuln-1.png)