import React from "react"
import {logo} from "../assets/index"
const Presentation = () => (
  <div id="presentation" className="text-white  font-comfortaa flex flex-col items-center p-24 xl:flex-row xl:justify-around ">
    <div className="flex flex-col bg-dimsecondBlue hover:bg-dimBlue m-4 p-4 rounded-lg">
      <p className="text-sm text-center p-4 xl:text-xl">bonjour, Je m'appelle Nabil AÏNAS</p>
      <p className="text-sm text-center pb-4">je suis étudiant en informatique et ingénieur Cloud ☁️</p>
      <p className="text-sm text-center pb-4">Adepte des pratiques DevOps ♾️</p>
    </div>
    <div>
      <img src={logo} alt="cube" height={200} width={400} className="xl:animate-bounce xl:p-0 "/>
    </div>
  </div>
)


export default Presentation